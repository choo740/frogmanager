To reset code first migration
1. dotnet ef database update 0 --context FrogAppContext
2. dotnet ef migrations remove --context FrogAppContext
3. dotnet ef migrations add InitialCreate --context FrogAppContext
4. dotnet ef database update --context FrogAppContext
