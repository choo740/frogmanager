using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore.Design;
using System;

namespace FrogDal.Models
{
     public class FrogAppContext : DbContext, IFrogAppContext, IDesignTimeDbContextFactory<FrogAppContext>
    {
        public DbSet<Frog> Frogs { get; set; }
        public DbSet<User> Users { get; set; }
        public FrogAppContext() : base(){

        }

        public FrogAppContext(DbContextOptions<FrogAppContext> options)
        : base(options)
        { 

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=frog.db");
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
        }

        public override DbSet<T> Set<T>(){
            return base.Set<T>();
        }

        public FrogAppContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<FrogAppContext>();
            optionsBuilder.UseSqlite("Data Source=frog.db");

            return new FrogAppContext(optionsBuilder.Options);
        }
    }

    public class Frog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public DateTime BirthAt { get; set; }
        [ForeignKey("Userid")]
        public virtual User User { get; set; }
    }

    public class User
    {
        [Key]
        public string id { get; set; }
        
        public virtual List<Frog> Frogs { get; set; }

        public User() {
            Frogs = new List<Frog>();
        }

    }

}