using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace FrogDal.Models
{
    public interface IFrogAppContext
    {
        DbSet<User> Users { get; set; }
        DbSet<Frog> Frogs { get; set; }
        DbSet<T> Set<T>() where T : class;
        DatabaseFacade Database { get; }
        int SaveChanges();
    }
}