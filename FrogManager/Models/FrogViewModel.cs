using System;
using System.ComponentModel.DataAnnotations;
using FrogManager.CustomValidationAttributes;
using FrogDal.Models;

namespace FrogManager.Models
{
    public class FrogViewModel
    {
        [Required]
        public int id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [Gender]
        public string Gender { get; set; }
        [Required]
        public DateTime BirthAt { get; set; }

        public FrogViewModel(){
            id = 0;
            Name = "";
            Gender = "";
            BirthAt = DateTime.Now;
        }
        
        public FrogViewModel(Frog frog){
            id = frog.id;
            Name = frog.Name;
            Gender = frog.Gender;
            BirthAt = frog.BirthAt;
        }
    }
}