﻿// Write your JavaScript code.
$(function () { // will trigger when the document is ready
    var myDatetimePicker = $('.datetimepicker').datetimepicker(
        {
            format: "YYYY-MM-DD hh:mm:ss",
        }
    ); //Initialise any date pickers
});