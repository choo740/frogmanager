
using System.ComponentModel.DataAnnotations;

namespace FrogManager.CustomValidationAttributes
{
    public class GenderAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {

            if(value.ToString() == "Male" || value.ToString() == "Female")
            {
                return ValidationResult.Success;
            }
            
            return new ValidationResult("Please enter a correct value");
        }
    }
}