using FrogDal.Models;
using System.Collections.Generic;
using FrogManager.Models;

namespace FrogManager.Services
{
    public interface IMyFrogManager
    {
        /// <summary>
        /// Reset the user's frog back to Romeo and Juliet
        /// </summary>
        /// <param name="userId"></param>
        void NewGame(string userId);

        /// <summary>
        /// Get all the frogs of a specific user.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        List<FrogViewModel> GetMyFrogs(string userId);

        /// <summary>
        /// Delete a frog
        /// </summary>
        /// <param name="frogId"></param>
        void DeleteFrog(int frogId);

        /// <summary>
        /// Retrieve frog info by frog id
        /// </summary>
        /// <param name="id">Frog id</param>
        /// <returns></returns>
        FrogViewModel GetFrogById(int id);

        /// <summary>
        /// Update a frog
        /// </summary>
        /// <param name="frog"></param>
        void UpdateFrog(FrogViewModel frog);

        /// <summary>
        /// Add a frog
        /// </summary>
        /// <param name="frog"></param>
        /// <param name="userId"></param>
        void AddFrog(FrogViewModel frog, string userId);

        /// <summary>
        /// Used to check whether the frog belongs to the user, for security purpose.
        /// </summary>
        /// <param name="frogId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        bool BelongsToUser(int frogId, string userId);
    }
}