using FrogManager.Repositories;
using FrogDal.Models;
using System;
using System.Linq;
using System.Collections.Generic;
using FrogManager.Models;

namespace FrogManager.Services
{
    //All the conversion from ViewModel to DbModels happens here
    //Complicated logics for data massaging, trans begin, trans end happens here
    public class MyFrogManager : IMyFrogManager
    {
        private readonly IFrogRepository _frogRepo;
        private readonly IUserRepository _userRepo;
        private readonly IFrogAppContext _frogAppContext;

        public MyFrogManager(IFrogAppContext frogAppContext, IFrogRepository frogRepo,
            IUserRepository userRepo)
        {
            _frogRepo = frogRepo;
            _userRepo = userRepo;
            _frogAppContext = frogAppContext;
        }

        public void NewGame(string userId)
        {

            using (var transaction = _frogAppContext.Database.BeginTransaction())
            {
                try
                {
                    User user;
                    DateTime start_time = DateTime.Now;

                    if (_userRepo.UserExist(userId))
                    {
                        user = _userRepo.GetUser(userId);
                        //clear all the user's existing frogs to ensure it is fresh start
                        _userRepo.ThrowAllMyFrogs(userId);
                    }
                    else
                    {
                        user = new User { id = userId };
                    }

                    user.Frogs.Add(new Frog
                    {
                        BirthAt = start_time,
                        Gender = "Male",
                        Name = "Remeo",
                        User = user
                    });

                    user.Frogs.Add(new Frog
                    {
                        BirthAt = start_time,
                        Gender = "Female",
                        Name = "Juliet",
                        User = user
                    });

                    _userRepo.AddOrUpdateUser(user);
                    _userRepo.SaveChanges();

                    transaction.Commit();
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    throw e;
                    // TODO: Handle failure
                }
            }
        }
        
        public FrogViewModel GetFrogById(int id){
            Frog frogInDb = _frogRepo.GetFrogById(id);
            return new FrogViewModel(frogInDb);
        }

        public void UpdateFrog(FrogViewModel frog){
            Frog frogInDb = _frogRepo.GetFrogById(frog.id);
            frogInDb.Name = frog.Name;
            frogInDb.Gender = frog.Gender;
            frogInDb.BirthAt = frog.BirthAt;

            _frogRepo.Update(frogInDb);
            _frogRepo.SaveChanges();
        }

         public void AddFrog(FrogViewModel frog, string userId){
            Frog newFrog = new Frog {
                Name = frog.Name,
                Gender = frog.Gender,
                BirthAt = frog.BirthAt,
                User = _userRepo.GetUser(userId)
            };

            _frogRepo.Add(newFrog);

            _frogRepo.SaveChanges();
        }

        public List<FrogViewModel> GetMyFrogs(string userId)
        {
            //no more lazy loading for ef core, has to get by eager loading
            return _frogRepo.MyFrogs(userId).Select(f => new FrogViewModel(f)).ToList();
        }

        public void DeleteFrog(int frogId) {
            _frogRepo.DeleteFrogById(frogId);
            _frogRepo.SaveChanges();
        }

        public bool BelongsToUser(int frogId, string userId){
            Frog frog = _frogRepo.GetFrogById(frogId);
            return (frog.User.id == userId);
        }
    }
}