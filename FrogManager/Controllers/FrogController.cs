using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using FrogManager.Models;
using FrogManager.Services;

namespace FrogManager.Controllers
{
    [Authorize]
    public class FrogController : Controller
    {
        private readonly IMyFrogManager _myFrogManager;
        private readonly UserManager<ApplicationUser> _userManager;
        
        public FrogController(IMyFrogManager myFrogManager, UserManager<ApplicationUser> userManager)
        {
            _myFrogManager = myFrogManager;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            string userId = _userManager.GetUserId(HttpContext.User);
            IEnumerable<FrogViewModel> myFrogs = _myFrogManager.GetMyFrogs(userId);
            return View(myFrogs);
        }

        public IActionResult Reset()
        {
            string userId = _userManager.GetUserId(HttpContext.User);
            _myFrogManager.NewGame(userId);
            return RedirectToAction("index");
        }

        [HttpGet]
        [Route("[Controller]/[Action]/{id}")]
        public IActionResult Edit(int id)
        {
            string userId = _userManager.GetUserId(HttpContext.User);
            if (_myFrogManager.BelongsToUser(id, userId) == false) {
                //If the user try to edit other user's frog
                return new ForbidResult();
            }

            FrogViewModel frog = _myFrogManager.GetFrogById(id);
            return View(frog);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("[Controller]/[Action]/{id}")]
        public ActionResult Edit(FrogViewModel frog)
        {
            string userId = _userManager.GetUserId(HttpContext.User);
            if (_myFrogManager.BelongsToUser(frog.id, userId) == false) {
                //If the user try to update other users frog
                return new ForbidResult();
            }

            if (ModelState.IsValid)
            {
                _myFrogManager.UpdateFrog(frog);
                return RedirectToAction("index");
            }
            return View(); 
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View(new FrogViewModel());
        }

        [HttpPost]  
        [ValidateAntiForgeryToken]
        public ActionResult Create(FrogViewModel frog)
        {
            if (ModelState.IsValid)
            {
                string userId = _userManager.GetUserId(HttpContext.User);
                _myFrogManager.AddFrog(frog, userId);
                return RedirectToAction("index");
            }
            return View(); 
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            _myFrogManager.DeleteFrog(id);
            return RedirectToAction("index", "Frog");
        }
    }
}