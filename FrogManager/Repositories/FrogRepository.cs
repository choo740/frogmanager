using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using FrogDal.Models;

namespace FrogManager.Repositories
{
    //All the database Frog Object db transactions is handled here
    public class FrogRepository: RepositoryBase<Frog>, IFrogRepository
    {
        public FrogRepository(IFrogAppContext frogAppContext) : base(frogAppContext) {

        }

        public void DeleteFrogById(int frogId) {
            Frog frog = _entity.Single(f => f.id == frogId);
            _entity.Remove(frog);
        }

        public void Update(Frog frog){
            _entity.Update(frog);
        }

        public void Add(Frog frog) {
            _entity.Add(frog);
        }

        public Frog GetFrogById(int id) {
           return _entity.Include(frog => frog.User).First(frog => frog.id == id);
        }

        public List<Frog> MyFrogs(string userId) {
            //eager loading user
            List<Frog> frogs = _entity.Include(f => f.User).Where(e => e.User.id == userId).ToList();
            return frogs;
        }
    }
}