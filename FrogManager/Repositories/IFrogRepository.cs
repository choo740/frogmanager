using FrogDal.Models;
using System.Collections.Generic;
using System;

namespace FrogManager.Repositories
{
    public interface IFrogRepository
    {
        Frog GetFrogById(int id);

        List<Frog> MyFrogs(string userId);

        int SaveChanges();

        void DeleteFrogById(int frogId);

        void Update(Frog frog);

        void Add(Frog frog);
    }
}