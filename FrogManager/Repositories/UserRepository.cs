using System.Linq;
using Microsoft.EntityFrameworkCore;
using FrogDal.Models;

namespace FrogManager.Repositories
{
    //All the database User Object db transactions is handled here
    public class UserRepository: RepositoryBase<User>, IUserRepository
    {
        public UserRepository(IFrogAppContext frogAppContext) : base(frogAppContext) {

        }

        public User GetUser(string id) {
            return _entity.Where(u => u.id == id).First();
        }

        public void ThrowAllMyFrogs(string userId) {
            User currentUser = _entity.Include(u => u.Frogs).Single(u => u.id == userId);
            currentUser.Frogs.Clear();
        }

        public User AddOrUpdateUser(User user) {
            if (UserExist(user.id)) {
                _entity.Update(user);
            }
            else {
                _entity.Add(user);
            }
            return user;
        }

        public bool UserExist(string id) {
            return _entity.Where(u => u.id == id).Count() > 0;
        }
    }
}