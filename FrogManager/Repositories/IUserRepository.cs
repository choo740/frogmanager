using FrogDal.Models;

namespace FrogManager.Repositories
{
    public interface IUserRepository
    {
        User GetUser(string id);

        User AddOrUpdateUser(User user);

        bool UserExist(string id);

        void ThrowAllMyFrogs(string userId);
        
        int SaveChanges();
    }
}