using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using FrogDal.Models;
using System;

namespace FrogManager.Repositories
{
    public abstract class RepositoryBase<T> where T : class
    {
        protected IFrogAppContext _frogAppContext {get; set;}

        protected DbSet<T> _entity {get; set;}
        
        public RepositoryBase(IFrogAppContext frogAppContext){       
            _frogAppContext = frogAppContext;
            _entity = _frogAppContext.Set<T>();
        }
        
        public List<T> All(){
            return _entity.ToList();
        }

        public int SaveChanges(){
            return _frogAppContext.SaveChanges();
        }

    }

}