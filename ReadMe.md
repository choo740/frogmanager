
## Prerequisite

1. Dotnet core SDK 2.1

2. npm

3. bower



## Steps to run the project

1. Open a new project folder.

2. run `git clone https://choo740@bitbucket.org/choo740/frogmanager.git`

3. run `cd FrogManager`

4. run `cd FrogManager`

6. run `npm install`

7. run `npm i -g bower`

8. run `bower install`

9. run `dotnet restore`

10. run `dotnet run` or `dotnet watch run` to auto rebundle when you modify the code



## To Refresh the Application DB (Holds Users Authentication Info)

1. `cd FrogManager`

2. `dotnet ef database update 0 --context ApplicationDbContext`

3. `dotnet ef database update --context ApplicationDbContext`



## To Refresh Frog DB (Holds User's Frogs)

1. `cd FrogManager`

2. `dotnet ef database update 0 --context FrogAppContext`

3. `dotnet ef database update --context FrogAppContext`



## Additional Info

1. Database: Sqlite

2. Client Script management: Bower

3. Client Script Bundle Minification: Gulp

4. DotNet Libraries: Nuget



## What does the app do?

1. Simply register and you'll receive two frogs by default. 
	- Remeo 🙆‍♂️ and Juliet 🙆‍♀️

2. You can then create, edit, delete your own frogs, or reset your frogs.

3. Email is configured to use a testing gmail account "frogmanager11@gmail.com", you can verify your email after register, and forgot password.